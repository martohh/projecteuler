def DigitCount(number):
  count = 0
  while number > 0:
    count += 1
    number /= 10
  return count

number = [1,1]
count = 3

while True:
  Fibonacci = number[0] + number[1]
  number[count%2] = Fibonacci
  digits = DigitCount(Fibonacci)

  print "%s %s"%(count, digits)
  count += 1

  if digits == 1000:
    break
