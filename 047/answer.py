import sys, os

limit = 999999
candidates = [1] * limit
primes = []

for i in range(2,limit):
	j = 2
	while j * i < limit:
		candidates[i*j] = 0
		j += 1

print "Printing primes"
count = 0
for i in candidates:
	if i == 1 and count > 1:
		primes.append(count)
	count += 1


def primeFactors(num):
	rtn = []
	for p in primes:
		if num < p:
			return rtn
		if num % p == 0:
			rtn.append(p)
	return rtn


tomatch = 4
inarow = 0
previous = 0

for n in range(1,limit):
	factors = primeFactors(n)
	if len(factors) == tomatch:
		print n,
		print factors

		if (n-1) == previous:
			inarow += 1
			previous = n
		else:
			inarow = 0
			previous = n

	if inarow == (tomatch-1):
		print n
		break



