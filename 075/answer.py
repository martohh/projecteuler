import sys, os


def isRightAngleTri(sides):
	if not len(sides) == 3:
		return None
	hyp = max(sides)
	sides.remove(hyp)
	a = sides[0]
	b = sides[1]
	sides.append(hyp)

	return pow(hyp,2) == pow(a,2) + pow(b,2)


def getSides(length):
	triangles = []
	rightAngleTri = []

	for i in range(1,length-1):
		for j in range(i+1,length):
			sides = sorted([i, j-i, length-j])
			if not sides in triangles:
				triangles.append(sides)
				if isRightAngleTri(sides) == True:
					rightAngleTri.append(sides)
					if len(rightAngleTri) > 1:
						return []
	return rightAngleTri

count = 0
for n in range(3,1500000):
	tri = []
	tri = getSides(n)
	if len(tri) == 1:
		count += 1
		print sum(tri[0]), 
		print tri

print "Solution: ",
print count
