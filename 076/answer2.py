

def calculateSum(number):
    position = 100
    summation = 0

    for b in bin(number)[::-1]:
        if b == "1":
            summation += position
        position -= 1
    return summation

for a in range(1000000):
    print calculateSum(a),
    print bin(a)
