import sys, os

limit = 1005000
candidates = [1] * limit

for i in range(2,limit):
	j = 2
	while j * i < limit:
		candidates[i*j] = 0
		j += 1

print "Printing primes"
count = 0
for i in candidates:
	if i == 1:
		pass
	count += 1

def circular(n):
	rtn = []
	rtn.append(n)
	for c in range(1,len(str(n))):
		n = str(str(n)[1:] + str(n)[:1])
		rtn.append(int(n))
	return rtn

def checkCircularPrime(n):
	for d in n:
		if candidates[int(d)] == 1:
			pass
		else:
			return False
	return True

print "Primed!"

c = 0
for n in range(2,1000000):
	if checkCircularPrime(circular(n)):
		print n, " all primes"
		c += 1

print "Answer: ", c
