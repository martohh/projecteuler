words_ones = ["zero","one","two","three","four","five","six","seven","eight","nine"]
words_teens = ["ten","eleven","twelve","thirteen","fourteen","fifteen","sixteen","seventeen","eighteen","nineteen"]
words_tens = ["zero","ten","twenty","thirty","forty","fifty","sixty","seventy","eighty","ninety"]

def digitIntoWord(number):
	rtn = ""
	number_tens = number % 100
	number_hundreds = (number - number_tens) / 100

	if number == 1000:
		return "one thousand"

	if number_hundreds > 0:
		rtn = rtn + words_ones[number_hundreds] + " hundred and "

	if number_tens % 10 == 0:
		rtn = rtn + words_tens[number_tens/10]
	elif number_tens > 19:
		rtn = rtn + words_tens[number_tens/10] + "-" + words_ones[number_tens%10]
	elif number_tens > 10:
		rtn = rtn + words_teens[number_tens%10]
	elif number_tens < 11:
		rtn = rtn + words_ones[number_tens%10]

	return rtn.replace("and zero","").replace("zero","")


buffer = ""
for n in range(0,1001):
	print len(str(digitIntoWord(n)).replace(" ","").replace("-","")), " ", digitIntoWord(n)
	buffer = buffer + digitIntoWord(n)

#print str(buffer).replace(" ","").replace("-","")
print len(str(buffer).replace(" ","").replace("-",""))
