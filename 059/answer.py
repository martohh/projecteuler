import sys, os, re, itertools
from operator import xor

f = open('cipher1.txt', 'r')
content = f.read()

comb = ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z']

for c in itertools.product(comb,comb,comb):
	cypher = c[0] + c[1] + c[2]
	message = ""
	pos = 0

	for letter in content.split(','):
		key = cypher[pos % 3]
		decypher = int(letter) ^ ord(key)
		message += chr(decypher)
		pos += 1
	sum = 0
	if ' the ' in message:
		for let in message:
			sum += ord(let)
		print cypher, message
		print "Sum: " + str(sum)

