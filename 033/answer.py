import sys, os, re

num = 123
den = 456

def dividWithoutDigit(d, num, den):

	if not (str(d) in str(num) and str(d) in str(den)):
		return False

	_num = str(num).replace(str(d),'')
	_den = str(den).replace(str(d),'')

	if _den == "" or _num == "":
		return False

	if float(_den) > 0 and float(den) > 0:
		if (float(num) / float(den)) == (float(_num) / float(_den)):
			if (float(num) / float(den)) < 1.0:
				print "Match"
				print float(num), '/', float(den), '=',
				print (float(num) / float(den))
				print float(_num), '/', float(_den), '=',
				print (float(_num) / float(_den))
				return True

	return False

def returnAllDigits(num, den):
	return set(str(num)) & set(str(den)) - set(['0'])

for num in range(1,100):
	for den in range(1,100):
		for d in returnAllDigits(num,den):
			if len(d) > 0:
				dividWithoutDigit(d, num, den)

