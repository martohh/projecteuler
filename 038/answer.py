
def FindProducts(n,number):
  product = []
  for _n in range(1,n+1):
    product.append(_n*number)
  return product

def NumDigits(num):
  count = 0
  while num > 0:
    count += 1
    num /= 10
  return count

def CountDigits(products):
  count = 0
  for product in products:
    while product > 0:
      count += 1
      product /= 10
  return count

def IsPanDigital(n,products):
  if not n == CountDigits(products):
    return False

  string = ""
  for product in products:
    string = "%s%s"%(string,str(product))

  for n in range(1,n+1):
    if not str(n) in string:
      return False

  return True

results = []
for n in range(1,100000):
  for a in range(2,10):
    products = FindProducts(a,n)
    if IsPanDigital(9,products):
      print products
      results.append(products)

print results
