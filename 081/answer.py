import sys, re, os, math

size = 80
grid = []
f = open("p081_matrix.txt","rw+")
for line in f:
	lineArray = []
	lineParts = line.split(',')
	for cell in lineParts:
		lineArray.append(int(cell))
	grid.append(lineArray)


order = []

for y in range(0,size):
	nestedArray = []
	for x in range(0,size):
		nestedArray.append(0)
	order.append(nestedArray)

for x in range(1,size):
	order[x][x] = int(math.pow((x+1),2))
	if x == 1: order[0][0] = 1

	number = int(math.pow((x+1),2)) - x - 1
	for y in range(x-1,-1,-1):
		order[y][x] = number
		number -= 1

	number = int(math.pow((x+1),2)) - 1
	for y in range(x-1,-1,-1):
		order[x][y] = number
		number -= 1


def getMinSum(x,y):
	above, beside, value = 9999999, 9999999, 0

	if y > 0: above = grid[y-1][x]
	if x > 0: beside = grid[y][x-1]

	if above > beside: value = beside
	if above < beside: value = above

	grid[y][x] = grid[y][x] + value

def getPosition(n):
	for x in range(0,size):
		for y in range(0,size):
			if order[y][x] == n:
				return [x,y]
	return [0,0]

def printGrid():
	for line in grid:
		for cell in line:
			print cell,
		print 

""" Calculate """
for n in range(1,int(math.pow(size,2))+1):
	if n % 10 == 0: print "round: ", n
	[x,y] = getPosition(n)
	getMinSum(x,y)

printGrid()
