def LenCycle(numerator,divisor):
  digits = []

  if numerator > divisor:
    numerator -= (numeator / divisor) * divisor

  while not numerator == 0:
    numerator *= 10
      
    digits.append(numerator/divisor)
    numerator -= (numerator / divisor) * divisor

    result = []
    for n in range(0,10):
      result = ParseArray(digits,n)
      if len(result) > 0:
        return result

  return []


def ParseArray(array, offset):
  if not (len(array) - offset) % 3 == 0:
    return []

  length = (len(array) - offset) / 3
  a = array[offset:offset+length]
  b = array[offset+length:offset+2*length]
  c = array[offset+2*length:offset+3*length]

  if CompareArrays(a,b,c) == True:
    return a

  return []


def CompareArrays(a,b,c):

  if not len(a) == len(b) or not len(a) == len(b) or not len(b) == len(c):
    return False

  for n in range(0,len(a)):
    if not a[n] == b[n]:
      return False
    if not b[n] == c[n]:
      return False
    if not a[n] == c[n]:
      return False

  return True


value = 0
longest = []
for n in range(1,1000):
  result = LenCycle(1,n)
  if len(result) > len(longest):
    longest = result
    value = n
    print n,": ",result


print "Answer: ", value, " ", len(longest), " ", longest
