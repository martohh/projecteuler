import sys
sys.path.append('/home/marty/scripts/projecteuler/lib/')
import common


#primes = common.LoadPrimes(100)
#print common.PrimeDivisors(144,primes)


results = []
for a in range(2,101):
  for b in range(2,101):
    results.append(pow(a,b))

print "Answer: ", len(set(results))
