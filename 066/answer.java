import java.util.*;

class answer {

	private static int limit_y = 9999999;

	public static double diophantine_x (int D, int y) {
		return Math.sqrt(1 + D * Math.pow(y,2));
	}

	public static double findMin_x (int D) {


		int cur_y = 1;
		int inc_y = 1;

		double cur_x = diophantine_x(D, cur_y);
		double next_x = diophantine_x(D, cur_y + inc_y);

		System.out.println("cur_x: " + cur_x + "\tnext_x: " + next_x);

		/*
		double min_x = -1.0;
		double prev_x = -1.0;
		double pos_x = -1.0;

		for (int y=1; y<limit_y; y+=1) {

			pos_x = diophantine_x(D, y);

			if ((pos_x % 1 == 0) && (min_x == -1.0 || pos_x < min_x))
				min_x = pos_x;
		}

		return min_x;
		*/

		return 0.0;
	}

	public static void main(String[] args) {

		double max_x = 0.0;
		double max_D = 0.0;

		for (int D = 2; D < 1000; D += 1) {

			double x = findMin_x(D);

			if (x > 0.0) {

				System.out.println("D: " + (double)D + "\tx: " + x );

				if (max_x < x) {
					max_D = D;
					max_x = x;
				}
			}
		}

		System.out.println("max_D: " + max_D + " max_x: " + max_x);
	}
}
