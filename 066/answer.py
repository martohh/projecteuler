import sys, re, os

x = lambda y, D: (1 + D*y**2)**0.5
y = lambda x, D: ((x**2 - 1) / D)**0.5



def findMinX(y_max, D):
	x_min = -1
	inc = 1
	y = 0

	while y < y_max:
		x_cur = x(y, D)
		if x_cur < x_min or x_min < 0:
			x_cur = x_min
			if x_cur < x_min:
				x_min = x_cur
		y += inc

	print x_min


print x(2,2)
print x(1,3)
print x(4,5)


findMinX(100, 35)


