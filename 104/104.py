import sys, os, re
import math
from decimal import Decimal
from math import sqrt

a, b = Decimal(1.0), Decimal(1.0)
golden_ratio = (1.0 + math.sqrt(5.0)) / 2.0
lookup = {}
lookup[0] = [0, a, b]


# Cache first 100
for n in range(0, 250):
	maxKey = max(lookup.keys(), key=int)
	[start, a, b] = lookup[maxKey]

	for i in range(start, n):
		a *= Decimal(golden_ratio)
		b *= Decimal(1.0 - golden_ratio)
		lookup[n] = [n, a, b]


def F_head(n, lookup):

	quickMulti(n, lookup)
	[n, a, b] = lookup[n]

	val = Decimal(a - b) / Decimal(math.sqrt(5.0))
	str_val = '{0:f}'.format(val)

	return str_val[0:9]


def quickMulti(n, lookup):
	_n = n
	keys = lookup.keys()
	multiKeys = []

	while _n > 1:
		for k in reversed(sorted(keys)):
			if _n >= k:
				_n -= k
				multiKeys.append(k)
				break
	a = Decimal(1.0)
	b = Decimal(1.0)
	for k in multiKeys:
		[_n, _a, _b] = lookup[k]
		a *= Decimal(_a)
		b *= Decimal(_b)

	lookup[n] = [n, a, b]


def Fibonacci(f_one, f_two):
	return [f_one + f_two, f_one]


def isPandigital(number):
	if not len(number) == 9:
		return False

	for n in range(1, 10):
		if not str(n) in str(number):
			return False
	return True


f_one = 1
f_two = 1
count = 2
limit = 10000000

for n in range(1,limit):
	[f_one, f_two] = Fibonacci(f_one, f_two)
	count += 1

	f_one = int(str(f_one)[-10:])
	f_two = int(str(f_two)[-10:])

	tail = str(f_one)[-9:]
	if isPandigital(tail):
		head = F_head(count, lookup)
		print count, "\t", head, tail, "tail is pandigital"
		
		if isPandigital(head):
			print "*** head is pandigital! ***"
			sys.exit()
