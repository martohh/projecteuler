import os, sys

def sumOfDigits(num):
	sum = 0
	for a in str(num): 
		sum += int(a)
	return sum

def truncateLastDigit(num):
	return str(num)[:-1]

def isDivisible(num,divisor):
	return (num % divisor) == 0

def primes(limit=1000):
	candidates = [1] * limit
	for a in range(2,limit):
		n = a
		while n+a < limit:
			n += a
			candidates [n] = 0

	count = 0
	primes = []
	for a in range(0,limit):
		if candidates[a] == 1:
			primes.append(count)
		count += 1

	return primes


print primes(pow(10,14))

