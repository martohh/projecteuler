
create table dates ( dates datetime );
truncate table dates;

select min(dates),max(dates),count(*) from dates;

insert into dates (dates) values ('1000-01-02');

insert into dates
select date_add(b.dates, interval a.number day)
from (
  select datediff(dates,(select min(dates) from dates)) + 1 number
  from dates
) a
join (
  select max(dates) dates
  from dates
) b;

-- How many Sundays fell on the first of the month during the twentieth century 
-- (1 Jan 1901 to 31 Dec 2000)?

select count(*)
from dates
where dates between '1901-01-01' and '2000-12-31'
and date_format(dates,'%a') = 'Sun'
and date_format(dates, '%e') = 1;

