# Problem 12
# going for speed

import sys, os, math, collections

divisors = {}
#divisors = collections.OrderedDict()

def findDivisors(n):
	a = math.floor(n/2) + 1
	rtn_divs = []

	while a > 0:
		if n % a == 0:
			if divisors.has_key(str(a)):
				rtn_divs += divisors[str(a)]
				n /= a
			else:
				rtn_divs.append(a)
				rtn_divs.append(n/a)
		a -= 1

	divisors[str(n)] = set(rtn_divs)
	return divisors[str(n)]


"""
 16.9s		10000
  6.5s		10000	- removed unnecessary casting
  6.0s		10000	- halved problem space

  0.1s		10000	- divide testing number by factor found in dictionary
  0.2s		20000	- 
  0.5s		40000
  1.2s		80000
  1.6s		100000
  6.4s		250000
  32.4s		750000
  49.6s		1000000
  200.4s	2500000
  1047.0s	7500000
  			10000000
			76576500
"""

num = 0
n = 1

while num < 100:

	num += n
	n += 1

	a = findDivisors(num)
	l = len(a)
	

	print str(num) + " " + str(a)
	
	"""
	if n % 100 == 0:
		print str(num) + " " + str(a)
	"""

	if l > 499:
		print str(n) + " " + str(l)
		break

	