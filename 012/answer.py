import sys, os


def genTriangleNumbers(number=0):
	n = 1
	value = 0
	triangleNumbers = []
	while n < number:
		value += n
		triangleNumbers.append(value)
		n += 1
	return triangleNumbers

def divisors(number):
	n = 1
	divs = []
	while n < number:
		if number % n == 0:
			divs.append(n)
		n += 1
	return divs


for n in genTriangleNumbers(100000):
	divs = divisors(n)
	print str(n) + "\t" + str(len(divs)) + "\t{",
	for div in divs:
		print div, 
	print "}"

	if len(divs) > 500:
		break
