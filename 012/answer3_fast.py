import sys, os, math, collections, itertools

divisors = {}

def findDivisors(n):
	a = math.floor(n/2) + 1
	rtn_divs = []

	while a > 0:
		if n % a == 0:
			if divisors.has_key(str(a)):
				rtn_divs += divisors[str(a)]
				n /= a
			else:
				rtn_divs.append(a)
				rtn_divs.append(n/a)
		a -= 1

	divisors[str(n)] = set(rtn_divs)
	return divisors[str(n)]

"""
  0.1s		10000	
  0.1s		20000	
  0.1s		40000
  0.1s		80000
  0.1s		100000
  0.1s		250000
  0.3s		750000
  0.3s		1000000
  0.6s		2500000
  1.4s		7500000
  1.9s		10000000
  4.3s		25000000
  12.0s		75000000
  12.4s		76576500
  			80000000
"""

flip = True
n = 1
a, b = 0, 0

while a*b < 80000000:
	a = n+1
	b = n

	if flip:
		a /= 2
	else:
		b /= 2

	flip != flip

	da = findDivisors(a)
	db = findDivisors(b)
	d = set(list(da) + list(db) + [(na * nb) for na in da for nb in db])
		
	if a*b % 50000 == 0:
		print str(a*b) + " " + str(d)
	
	n += 1

	if len(d) > 499:
		print str(a*b) + " " + str(d)
		break
