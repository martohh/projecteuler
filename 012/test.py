import sys
sys.path.append('/home/marty/scripts/projecteuler/lib/')
import common
import time

maxRange = 50000

primeticks = 0
ticks = time.time()
print "Loading Primes"
primes = common.LoadPrimes(maxRange)
print "Done Loading Primes"
primeticks = time.time() - primeticks

fasttick = 0
slowtick = 0

for n in range(1,maxRange):
  if n % 1000 == 0:
    print n

  ticks = time.time()
  a = common.FindDivisors(n)
  slowtick += time.time() - ticks

  ticks = time.time()
  b = common.FastFindDivisors(n,primes)
  fasttick += time.time() - ticks

  if not len(a) == len(b):
    print n

print "Summary:"
print "Prime: ", primeticks
print "Fast: ", fasttick
print "Slow: ", slowtick

