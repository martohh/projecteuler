import os, sys, math

n = 0
count = 0
limit = 100

for (num, n) in [(num, n) for num in range(1,limit) for n in range(1,limit)]:
  
  if len(str(pow(num,n))) == n:
    print num, n
    count += 1

print count