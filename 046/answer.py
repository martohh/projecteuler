import sys, itertools, math
sys.path.append('/home/marty/scripts/projecteuler/lib/')
import common

print "Loading Primes"
maxPrime = 100000
primes = common.LoadPrimes(maxPrime)
print "Primed!"


def isOddComposite(number):
	if number in primes:
		return False
	if number % 2 == 0:
		return False
	return True


def checkSolution(number,prime):
	if number < prime:
		return False
	findSquare = math.sqrt((number-prime)/2)
	if findSquare == math.floor(findSquare):
		return True
	return False


composite = 21
nosolution = []
for composite in range(0,maxPrime):
	if isOddComposite(composite):
		solutions = 0

		for prime in primes:

			if checkSolution(composite, prime):
				print composite, " ",prime, "+ 2x",str(math.sqrt((composite-prime)/2)), "^2"
				solutions += 1
		if solutions == 0:
			print "Cannot find solution for ", composite
			nosolution.append(composite)

print nosolution
