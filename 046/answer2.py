import sys, os

primes = [1]*pow(10,4)

for a in range(2,len(primes)):
	b = a
	while b+a < len(primes):
		b += a
		primes[b] = 0

productPrimes = []
for n in range(len(primes)):

	if n % 100 == 0:
		print "testing " + str(n)

	if n % 2 == 0:
		continue

	# If a prime, shortlist it
	if primes[n] == 1:
		productPrimes.append(n)

	# Composite number
	else:
		solution = False
		for p in productPrimes:
			if solution:
				break

			for a in range(1,n):
				if n == int(p) + 2 * pow(a,2):
					solution = True
					break
				if n < int(p) + 2 * pow(a,2):
					break

		if solution == False:
			print str(n)
			sys.exit()
