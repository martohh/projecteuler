import os, sys

def double_digit( carry=0, index=0 ):
	global digit

	if index >= len( digit ):
		return

	value = digit[index] * 2
	digit[index] = value % 10 + carry
	_carry = value / 10

	if index+1 < len( digit ):
		double_digit( _carry, index+1 )
	else:
		digit.append(_carry)


def remove_leading_zero():
	global digit
	n = len(digit)
	while n >= 0:
		n -= 1
		if digit[n] > 0:
			return
		digit.pop()


def print_digit():
	global digit
	n = len(digit)
	number = ""

	while n > 0:
		if n % 3 == 0:
			number += str(',')
		number += str(digit[n-1])
		n -= 1

	print number


def sum_digits():
	global digit
	sum = 0
	for d in digit:
		sum += d
	return sum


digit = [1]

for x in range (1, 1001):
	
	double_digit(0,0)
	remove_leading_zero()
	print_digit()
	print sum_digits()
	print

