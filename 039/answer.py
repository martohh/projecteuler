import os, sys

def isSolution(a,b,c):
	return pow(a,2) + pow(b,2) == pow(c,2)


answers = []
max_sol = 0
max_p = 0

for p in range(1001):

	solutions = []
	for a in range(1,p):
		for b in range(1,p-a):
			c = p - a - b
			if isSolution(a,b,c):
				solution = [a,b,c]
				solution = sorted(solution)
				if not solution in solutions:
					solutions.append(solution)
	if len(solutions) > max_sol:
		max_sol, max_p = len(solutions), p

	answers.append([p,len(solutions)])

print answers
print max_sol, max_p
