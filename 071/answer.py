

def gcd(a, b):
    while a != b:
        if a > b:
            a = a - b
        else:
            b = b - a
    return a


class fraction():
    def __init__(self, _num=None, _den=None):
        if _num is None:
            self.num = 0
        else:
            self.num = _num
        if _den is None:
            self.den = 1
        else:
            self.den = _den

    def evaluate(self):
        if self.den > 0:
            return float(self.num) / float(self.den)
        else:
            return -1

    def reduce(self):
        _gcd = gcd(self.num, self.den)
        while _gcd > 1:
            self.num /= _gcd
            self.den /= _gcd
            _gcd = gcd(self.num, self.den)

    def __str__(self):
        return str(self.num) + "/" + str(self.den)

    def __lt__(self, other):
        # return (float(self.num) / float(self.den)) < (float(other.num) / float(other.den))
        return self.evaluate() < other.evaluate()

    def __gt__(self, other):
        return self.evaluate() > other.evaluate()

LIMIT = 1000000
TARGET = fraction(3, 7)
MAX = fraction(0, 1)

for _a in range(1, LIMIT):
    if _a % 1000 == 0:
        print _a
    for _b in range(1, _a):
        a = fraction(_b, _a)
        a.reduce()
        if a < TARGET and a > MAX:
            MAX = fraction(a.num, a.den)

print "Answer to question 071"
print MAX,
print MAX.evaluate()
print float(2) / float(5)
