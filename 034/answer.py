import math

def DigitalFactorial (number):
  total = 0
  _number = number
  while _number > 0:
    digit = _number % 10
    _number /= 10
    total += math.factorial(digit)
  return number == total

def DigitalFactorialSum (number):
  total = 0
  _number = number
  while _number > 0:
    digit = _number % 10
    _number /= 10
    total += math.factorial(digit)
  return total

for n in range(1,pow(10,8)):
#  print "%s %s"%(n,DigitalFactorialSum(n))
  if DigitalFactorial(n):
    print n
