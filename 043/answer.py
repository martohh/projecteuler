import sys, itertools
sys.path.append('/home/marty/scripts/projecteuler/lib/')
import common

result = []

for n in itertools.permutations(["1","2","3","4","5","6","7","8","9","0"]):
  a = ''.join(n[1:4])
  b = ''.join(n[2:5])
  c = ''.join(n[3:6])
  d = ''.join(n[4:7])
  e = ''.join(n[5:8])
  f = ''.join(n[6:9])
  g = ''.join(n[7:10])

  if not int(a) % 2 == 0:
    continue
  if not int(b) % 3 == 0:
    continue
  if not int(c) % 5 == 0:
    continue
  if not int(d) % 7 == 0:
    continue
  if not int(e) % 11 == 0:
    continue
  if not int(f) % 13 == 0:
    continue
  if not int(g) % 17 == 0:
    continue

  print n
  result.append(n)

print result
total = 0
for n in result:
  total += int(''.join(n))
print "Sum: ", total
