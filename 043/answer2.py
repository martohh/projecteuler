import itertools, sys

perm = []
perm = list(itertools.permutations([0,1,2,3,4,5,6,7,8,9]))

solution = []
sum = 0
for p in perm:
	num = "".join(map(str,p))
	if int(num[1:4]) % 2:
		continue
	if int(num[2:5]) % 3:
		continue
	if int(num[3:6]) % 5:
		continue
	if int(num[4:7]) % 7:
		continue
	if int(num[5:8]) % 11:
		continue
	if int(num[6:9]) % 13:
		continue
	if int(num[7:10]) % 17:
		continue

	print num
	sum += int(num)

print sum