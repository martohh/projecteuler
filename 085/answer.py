print 36 * 77

def countRec(a, b):
    _sum = 0
    for _a in range(a, 0, -1):
        for _b in range(b, 0, -1):
            _sum += _a * _b
    return _sum

TARGET = 2000000
diff = 9999
a_min = 0
b_min = 0


for a in range(1, 200):
    print a 
    for b in range(1, 200):
        recs = countRec(a, b)
        _diff = abs(recs - TARGET)
        if _diff < diff:
            a_min = a
            b_min = b
            diff = _diff


print a_min, b_min, diff

