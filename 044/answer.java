import java.util.*;

class answer {

	private static int limit = 99999999;
	private static BitSet pentagonals = new BitSet(limit);

	public static void calcPentagonals () {

		/* Init primes array of boolean */
		for (int i=0; i<limit; i+=1) 
			pentagonals.clear(i);

		/* Calculate pentagonals */
		for (int i=1; i<(int)Math.sqrt(limit); i+=1)
			pentagonals.set(pentagonal(i));
	}

	public static void printPentagonals () {
		for (int i=0; i<limit; i++)
			if (pentagonals.get(i))
				System.out.println(i);
	}

	public static int pentagonal (int number) {
		return (number * (3 * number - 1) / 2);
	}

	public static void main(String[] args) {
		System.out.println("problem 44");

		calcPentagonals();

		for (int i=1; i<limit; i+=1) {
			
			if (!pentagonals.get(i))
				continue;

			for (int j=1; j<i; j+=1) {

				if (!pentagonals.get(j))
					continue;

				int sum = i + j;
				int diff = i - j;
				int abs = Math.abs(i-j);

				if (!pentagonals.get(sum) || !pentagonals.get(diff))
					continue;

				System.out.println(i + " " + j);
				System.out.println("sum:" + sum + " diff:" + diff + " abs:" + abs);
			}
		}
	}
}
