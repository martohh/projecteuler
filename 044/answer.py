import itertools, math

def Pentagonal(n):
  return n * (3 * n - 1) / 2


P = []

print "Generating Pentagonal Numbers...",
n = 1
while n < pow(2,20):
  P.append(Pentagonal(n))
  n += 1
print "Done"

print "Calculating...",
for a,b in itertools.product(P,P):
  if not a == b:
    add = math.fabs(a + b)
    sub = math.fabs(a - b)
    if add in P and sub in P:
      print a, " ", b
print "Done"
