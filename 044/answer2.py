import os, sys, itertools

pentagonal = lambda n: n*(3*n-1)*0.5

n = range(1,10000)
p = map(pentagonal,n)

def isSumPentagonal(a ,b):
	return (a+b) in p

def isDiffPentagonal(a, b):
	return abs(a-b) in p

for r in itertools.combinations(p,2):
	if isSumPentagonal(r[0], r[1]) and isDiffPentagonal(r[0], r[1]):
		print r
