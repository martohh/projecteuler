import sys, os

def Triangle(n):
  a = float(n * (n + 1 ) / 2)

  return a

def Pentagonal(n):
  a = float(n * (3 * n - 1) / 2)
  return a

def Hexagonal(n):
  a = float(n * (2 * n - 1))
  return a

numbers = [0]*100000000

aa, bb, cc = 0,0,0
a, b, c = 0,0,0

while aa < len(numbers):
  numbers[aa] += 1
  a += 1
  aa = Triangle(a)

while bb < len(numbers):
  numbers[bb] += 1
  b += 1
  bb = Pentagonal(b)

while cc < len(numbers):
  numbers[cc] += 1
  c += 1
  cc = Pentagonal(c)

for n in range(len(numbers)):
  if numbers[n] == 3:
    print str(n) + " " + str(numbers[n])

