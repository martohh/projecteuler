# Problem 50

import sys, os, itertools

"""
  10^3  0.2s
  10^4  0.3s
  10^5  14.2s
  10^6  2011.0s
"""
primes = [1]*pow(10,6)

for a in range(2,len(primes)):
  for b in range(2,len(primes)/a):
    if a*b < len(primes):
      primes[a*b] = 0

print "Primed"

primeNumbers = []
maxPrime = 0
maxRun = 0

for n in range(len(primes)):
  if primes[n] == 1:
    primeNumbers.append(n)
print "Created Primes",
print len(primeNumbers)

for a in range(2,len(primeNumbers)):
  summation = 0
  run = []

  for b in range(a,len(primeNumbers)):
    
    summation += primeNumbers[b]

    if summation > len(primes):
      break

    if summation in primeNumbers:

      if maxRun < len(run):
        maxPrime = summation
        maxRun = len(run)

    
    run.append(primeNumbers[b])

print maxPrime
