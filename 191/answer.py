# 191

import re, sys, os, itertools

code = ['O', 'A', 'L']
absent = lambda code: not re.search(r'AAA',code) == None
late = lambda code: code.count('L') > 1

def relax(code):
	if len(code) < 5: return None

	firstTwoChar = code[:2]
	lastTwoChar = code[-2:]
	characterLen = sum([len(a) for a in re.findall(r'[a-zA-Z]+',code)])
	digitSum = sum([int(a) for a in re.findall(r'(\d+)',code)])

	return str(firstTwoChar) + str(characterLen + digitSum - 4) + str(lastTwoChar)


# Generate attendance for 5 days
attendance = {}
for a in ["".join([a,b,c,d,e]) for a,b,c,d,e in itertools.product(code,code,code,code,code)]:

	if absent(a): continue
	if late(a): continue

	relaxed = relax(a)
	if relaxed in attendance: attendance[relaxed] += 1
	else: attendance[relaxed] = 1


# Cross product short attendance on itself, generate codes for 10 days
for n in range(2):
	for a,b in [(a,b) for a,b, in itertools.product(attendance,attendance)]:
		
		c = str(a) + str(b)
		if absent(c): continue
		if late(c): continue
		c = relax(c)
		
		if c in attendance: attendance[c] += int(attendance[a]) * int(attendance[b])
		else: attendance[c] = int(attendance[a]) * int(attendance[b])


# Cross product of 20 Day and 10 Day codes
for a,b in [(a,b) for a in attendance for b in attendance]:

	if not re.search(r'16',a): continue
	if not re.search(r'6',b): continue

	c = str(a)+str(b)
	if absent(c): continue
	if late(c): continue

	c = relax(c)
	if c in attendance: attendance[c] += int(attendance[a]) * int(attendance[b])
	else: attendance[c] = int(attendance[a]) * int(attendance[b])


# Tallying final count
count = 0
for a in sorted(attendance):
	if not re.search(r'26',a): continue
	print a,
	print attendance[a]
	count += int(attendance[a])

print "count: ",
print str(count)

