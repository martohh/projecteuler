def CollatzProblem(number):
  if number % 2 == 1:
    return 3*number + 1
  else:
    return number / 2

longestChain = []


for number in range(1,1000000):

  if number % 10000 == 0:
    print number

  chain = []
  chain.append(number)

  while number > 1:
    number = CollatzProblem(number)
    chain.append(number)

  if len(chain) > len(longestChain):
    longestChain = []
    longestChain = chain

print longestChain
