def SquareOfDigits(number):
  total = 0
  while number > 0:
    total += pow(number % 10, 2)
    number /= 10
  return total

result = []

for n in range (1,pow(10,7)):
  value = n
  chain = []
  while True:
    if value == 1 or value == 89:
      if value in chain:
        break
    value = SquareOfDigits(value)
    chain.append(value)
  result.append([n,value])
  print n, " ", value

count = 0
for a,b in result:
  if b == 89:
    count += 1
    print "Number: ",a, " First Repeat: ",b

print "Answer: ",count
