import random
from decimal import *

def Dice(number,sides):
  sum = 0
  for x in range(0,number):
    sum += random.randint(1,sides)
  return sum


limit = pow(2,19)
games = 0
pete = 0
for x in range(0,limit):
  if Dice(4,4) > Dice(6,6):
    pete += 1
  games += 1

  print pete, ":", games, " ", Decimal(pete)  / Decimal(games), " ", Decimal(100.0) * Decimal(x) / Decimal(limit), "%"
