square = 5000
limit = square**2 + 1
candidates = [1] * limit
squares = {}

# Calculate primes
print "Calculating Primes... ",
for n in range(2, limit):
    a = n
    while a+n < limit:
        a += n
        candidates[a] = 0
print "Done."

# Print primes
buffer = ""
f = open('primes' + str(square) + '.txt', 'w')
for n in range(0, limit):
    if candidates[n] == 1:
        buffer += " " + str(n)
    if len(buffer) > 100:
        f.write(buffer + "\n")
        buffer = ""
f.write(buffer)
f.close

print "Setting up squares... ",
for n in range(1, limit, 2):
    squares[str(n)] = []
print "Done."

print "Placing Numbers... ",
s = 1
for n in range(1, limit):
    if squares[str(s)] is None:
        squares[str(s)] = []
    squares[str(s)].append(n)
    if n == s**2:
        s += 2
print "Done."

nonPrime = 1
prime = 0
f = open('output.txt', 'w')
buffer = ""
ratio = ""
_ratio = ""

# Print square
for k in range(1, limit, 2):
    l = len(squares[str(k)])
    _l = l
    if l > 0:
        # print k, l, squares[str(k)]

        # Calculate corners
        while l > 1:
            corner = squares[str(k)][l-1]
            l -= k - 1

            if candidates[corner] == 1:
                # print str(corner) + " is a prime diagonal"
                prime += 1
            else:
                # print str(corner) + " is NOT prime"
                nonPrime += 1

    _ratio = str(float(prime) / float(prime + nonPrime) * 100)

    if ratio != _ratio:
        ratio = _ratio
        buffer = "%s\t%s\t%s\t%s\t%s\n" % (str(k), str(_l), str(prime), str(nonPrime), str(ratio))
        f.write(str(buffer))

f.close()
print buffer
