import os, sys, random, itertools

print "problem 41"

def isPandigital(value):
	for n in range(1,len(str(value))+1):
		if not str(n) in str(value):
			return False
	return True

def isPrime(value):
	for n in range(2,value):
		if value % n == 0:
			return False
	return True

digits = []
for n in range(1,9):
	digits.append(n)
	for nList in list(itertools.permutations(digits,n)):
		num = ""
		for n in nList:
			num += str(n)
		if isPrime(int(num)):
			print str(num) + " is prime!"
