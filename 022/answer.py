import sys, os

def score_name(name):
	sum = 0
	for char in name:
		sum += int(ord(char)-64)
	return sum

f = open('p022_names.txt', 'r')
count = 0

lines = f.readlines()
lines = lines[0].split(',')
lines = sorted(lines)

count = 0
score = 0

for line in lines:
	count += 1
	line = line.replace("\"","")
	score += (int(count) * int(score_name(line)))
	print "Line:\t" + str(count) + "\tName Score: " + str(score_name(line)) + "\tScore: " + str(int(count) * int(score_name(line)))  + "\tName: " + str(line)

print str(score)

