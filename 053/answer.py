"""
import itertools
for n in itertools.combinations('12345',3):
  print "".join(n)
"""

import math

def combination(n,r):
  return math.factorial(n) / (math.factorial(r) * math.factorial(n-r))

results = []

for n in range(0,101):
  for r in range(0,n+1):
    comb = combination(n,r)
    if comb > pow(10,6):
      print comb
      results.append(comb)

print "Answer: ",len(results)
