
def isPalindromic(number):
  a = []

  while number > 0:
    a.append(number % 10)
    number /= 10
  
  for i in range(0,len(a)-1):
    if a[i] != a[len(a)-1-i]:
      return False
  return True


c = []
for a in range(100,999):
  for b in range (100,999):
    if (isPalindromic(a*b)):
      c.append(a*b)

c = sorted(c)
print c
