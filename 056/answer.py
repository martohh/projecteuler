def SumDigits(number):
  total = 0
  while number > 0:
    total += number % 10
    number /= 10
  return total

MaxSum = 0
for a in range(1,100):
  for b in range(1,100):
    number = pow(a,b)
    total = SumDigits(number)
    if MaxSum < total:
      MaxSum = total

print MaxSum
