import sys
if len(sys.argv) == 2:
  problem = sys.argv[1]
else:
  problem = 1

def LoadPuzzle(num):
  puzzle = []
  if num < 10:
    num = "0" + str(num)
  f = open("sudoku.txt","r")
  for line in f:
    if "Grid " + str(num) in line:
      print line
      for line in f:
        if "Grid" in line:
          f.close()
          return puzzle
        row = []
        for num in line.strip():
          row.append(int(num.replace("\n","")))
        puzzle.append(row)
  f.close()
  return puzzle


def GetRow(num,puzzle):
  return puzzle[num]


def GetCol(num,puzzle):
  col = []
  for n in range(0,9):
    col.append(puzzle[n][num])
  return col


def GetSqr(num,puzzle):
  l_row = 3 * (num / 3)
  h_row = 3 * (1 + num / 3)
  l_col = 3 * (num % 3)
  h_col = 3 * (1 + num % 3)

  line = []
  for r in range(l_row, h_row):
    for c in range(l_col, h_col):
      line.append(puzzle[r][c])
  return line


def GetClues(row,col,puzzle):
  clues = []
  clues.extend(GetRow(row,puzzle))
  clues.extend(GetCol(col,puzzle))
  num = 3 * (row / 3) + (col / 3)
  clues.extend(GetSqr(num,puzzle))

  rtnClues = []
  for n in range(1,10):
    if n not in clues:
      rtnClues.append(n)
  return sorted(set(rtnClues))

def PlaceNumber(num,puzzle):
  """ Place Trivial Number in Square """
  moves = False
  l_row = 3 * (num / 3)
  h_row = 3 * (1 + num / 3)
  l_col = 3 * (num % 3)
  h_col = 3 * (1 + num % 3)

  for n in range(1,10):
    position = []
    for row in range(l_row,h_row):
      for col in range(l_col, h_col):
        if puzzle[row][col] == 0:
          if n in GetClues(row,col,puzzle):
            if len(position) > 1:
              break
            else:
              position.append([row,col])
    if len(position) == 1:
      puzzle[position[0][0]][position[0][1]] = n
      moves = True

    return moves


def Guess(puzzle):
  for a in range(0,9):
    for b in range(0,9):
      if puzzle[a][b] == 0:
        for clue in GetClues(a,b,puzzle):
          print a," ", b, " ", clue
          PrintPuzzle(puzzle)
          """ Make the First Guess """
          _puzzle = PlaceGuess(a,b,clue,puzzle[:])
          if len(_puzzle) > 0:
            return _puzzle
  return

def PlaceGuess(row,col,num,puzzle):
  puzzle[row][col] = num
  puzzle = Trivial(puzzle)
  if ValidPuzzle(puzzle):
    return puzzle
  return []


def PrintPuzzle(puzzle):
  for line in puzzle:
    print line

def ValidPuzzle(puzzle):
  for n in range(0,9):
    if not sum(GetRow(n,puzzle)) == 45:
      return False
    if not sum(GetCol(n,puzzle)) == 45:
      return False
    if not sum(GetSqr(n,puzzle)) == 45:
      return False
  return True

def Trivial(puzzle):
  while not ValidPuzzle(puzzle):
    moves = 0
    for a in range(0,9):
      for b in range(0,9):
        if puzzle[a][b] == 0:
          clues = GetClues(a,b,puzzle)
          sqr = 3 * (a / 3) + (b / 3)
          print "sqr: ",sqr, " row: ",a, " col: ",b, " ", clues
          if len(clues) == 1:
            moves += 1
            puzzle[a][b] = clues[0]
    for n in range(0,9):
      if PlaceNumber(n,puzzle):
        moves += 1
    if moves > 0:
      raw_input("pause")
      PrintPuzzle(puzzle)
    else:
      break
  return puzzle



"""
  Sudoku Solver
"""

puzzle = []
puzzle = LoadPuzzle(problem)

puzzle = Trivial(puzzle)

if ValidPuzzle(puzzle):
  print
  print "Solved!"
else:
  print
  print "Impass"
  puzzle = Guess(puzzle)
  
