import itertools, sys

perm = []
perm = list(itertools.permutations([1,2,3,4,5,6,7,8,9]))

solution = []
products = []
for p in perm:
	for a in range(1,10):
		for b in range(1,10-a):
			c = 9 - a - b
			num = "".join(map(str,p))

			multiplicand = "".join(map(str,p[0:a]))
			multiplier = "".join(map(str,p[a:b]))
			product = "".join(map(str,p[b:9]))

			if len(multiplicand) == 0 or len(multiplier) == 0 or len(product) == 0:
				continue

			if int(multiplicand) * int(multiplier) == int(product):
				print multiplicand, multiplier, product
				answer = []
				answer = sorted([multiplicand, multiplier, product])
				products.append(product)
				if not answer in solution:
					solution.append(answer)

print len(solution), 
print solution

sum = 0
for s in set(products):
	sum += int(s)

print sum


