def IsPandigital(Pandigital,multiplicand,multiplier,product):
  for n in range(1,Pandigital+1):
    if not str(n) in str(multiplicand) and not str(n) in str(multiplier) and not str(n) in str(product):
      return False
    if str(multiplicand).count(str(n)) + str(multiplier).count(str(n)) + str(product).count(str(n)) > 1:
      return False
  return True

def CountDigits(plicand,plier,prod):
  return len(str(plicand)) + len(str(plier)) + len(str(prod))

prod = 1
plier = 0
plicand = 0
results = []
total = 0

while prod < pow(10,5):
  plier = prod - 1
  while plier > 0:
    if prod % plier == 0:
      plicand = prod/plier
      if plier < plicand:
        break
      p = CountDigits(plier,plicand,prod)
      if IsPandigital(p,plier,plicand,prod) and p == 9:
        print "Pandigital: %s Product: %s Multiplier: %s Multiplicand: %s"%(p,prod,plier,plicand)
        results.append([p,plier,plicand,prod])
        total += prod
    plier -= 1
  prod += 1

print results
print total
