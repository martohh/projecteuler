
def IsMultiple(number):
  if number % 3 == 0 or number % 5 == 0:
    return True
  return False

total = 0
for n in range(1,1000):
  if IsMultiple(n):
    total += n

print total
