size = 21
grid = []


def printGrid():
  for x in range(0,size):
    print grid[x]

for x in range(0,size):
  row = []
  for y in range(0,size):
    row.append(0)
  grid.append(row)

for x in range(0,size):
  grid[x][0] = 1
  grid[0][x] = 1

for y in range (1,size):
  for x in range (1,size):
    grid[x][y] = grid[x][y-1] + grid[x-1][y]

printGrid()
print grid[size-1][size-1]
