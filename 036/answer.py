import sys, os


def isPalindromic(number):
	if not type(number) == str:
		str_num = str(number)
	else:
		str_num = number

	a = 0
	b = len(str_num) - 1 

	while a < b:
		if not str_num[a] == str_num[b]:
			return False
		a += 1
		b -= 1
	return True


dec_num = 0
solution = []
while dec_num < 1000000:
	bin_num = str(bin(dec_num))[2:]
	if isPalindromic(dec_num) and isPalindromic(str(bin_num)):
		solution.append([dec_num, bin_num])
	dec_num += 1

sum = 0
for s in solution:
	print s
	sum += s[0]

print sum
