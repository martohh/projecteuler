
def IsPrime(number):
  if number % 2 == 0:
    return False

  for i in range(2,number-1):
    if number % i == 0:
      return False

  return True

c = 0
num = 0
p = []
while c < 10010:
  if IsPrime(num):
    p.append(num)
    c += 1
    if c % 1000 == 0:
      print "%s:%s"%(c,num)
  num += 1

print p[0:10]
print p[6]
print p[10000:10020]
print 
print p[10000] # indexing is less one -1
