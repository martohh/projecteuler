player1 = []
player2 = []

def highCard(hand):
  cards = []
  for n in range(0,5):
    card = hand[n][0]
    card = card.replace("T","10")
    card = card.replace("J","11")
    card = card.replace("Q","12")
    card = card.replace("K","13")
    card = card.replace("A","14")
    cards.append(int(card))
  return max(cards)


def pairValue(hand):
  cards = []
  for n in range(0,5):
    card = hand[n][0]
    card = card.replace("T","10")
    card = card.replace("J","11")
    card = card.replace("Q","12")
    card = card.replace("K","13")
    card = card.replace("A","14")
    cards.append(int(card))
  for n in range(0,15):
    if cards.count(n) == 2:
      return n
  return 0


def isFlush(hand):
  suit = hand[0][1]
  for n in range(1,5):
    if not hand[n][1] == suit:
      return False
  return True


def isStraight(hand):
  cards = []
  for n in range(0,5):
    card = hand[n][0]
    card = card.replace("T","10")
    card = card.replace("J","11")
    card = card.replace("Q","12")
    card = card.replace("K","13")
    card = card.replace("A","14")
    cards.append(int(card))
  cards = sorted(cards)
  for n in range(0,4):
    if not (cards[n]+1) == cards[n+1]:
      return False
  return True


def isThreeOfKind(hand):
  cards = []
  for n in range(0,5):
    cards.append(hand[n][0])
  for n in range(0,5):
    if cards.count(cards[n]) == 3:
      return True
  return False


def isTwoPair(hand):
  values = ["2","3","4","5","6","7","8","9","T","J","Q","K","A"]
  cards = []
  pairs = 0
  for n in range(0,5):
    cards.append(hand[n][0])
  for n in range(0,12):
    if cards.count(values[n]) == 2:
      pairs += 1
  if pairs == 2:
    return True
  return False


def isFourOfKind(hand):
  cards = []
  for n in range(0,5):
    cards.append(hand[n][0])
  for n in range(0,5):
    if cards.count(cards[n]) == 4:
      return True
  return False


def isPair(hand):
  cards = []
  for n in range(0,5):
    cards.append(hand[n][0])
  for n in range(0,5):
    if cards.count(cards[n]) == 2:
      return True
  return False


def isFullHouse(hand):
  if isPair(hand):
    if isThreeOfKind(hand):
      return True
  return False


def isRoyalFlush(hand):
  cards = []
  RoyalFlush = ["T","J","Q","K","A"]
  if isFlush(hand):
    for n in range(0,5):
      cards.append(hand[n][0])
    if set(cards) == set(RoyalFlush):
      return True
  return False


def isStraightFlush(hand):
  if isFlush(hand):
    if isStraight(hand):
      print "Is Straight Flush"

def handValue(hand):
  if isRoyalFlush(hand):
    return 9
  if isStraightFlush(hand):
    return 8
  if isFourOfKind(hand):
    return 7
  if isFullHouse(hand):
    return 6
  if isFlush(hand):
    return 5
  if isStraight(hand):
    return 4
  if isThreeOfKind(hand):
    return 3
  if isTwoPair(hand):
    return 2
  if isPair(hand):
    return 1
  return 0

""" Read file and prepare hands """
f = open('poker.txt','r')
for line in f:
  cells = line.replace("\n","").split(" ")
  hand = []
  for n in range(0,5):
    hand.append(cells[n])
  player1.append(hand)
  hand = []
  for n in range(5,10):
    hand.append(cells[n])
  player2.append(hand)


""" Main Code """
win1 = 0
ties = 0

for n in range(0,len(player1)):
  value1 = handValue(player1[n])
  value2 = handValue(player2[n])

  if value1 > value2:
    win1 += 1

  elif value1 == 0 and value2 == 0:
    high1 = highCard(player1[n])
    high2 = highCard(player2[n])

    if high1 > high2:
      win1 += 1

  elif value1 == value2:
    """ Players both have one pair"""
    if value1 == 1 and value2 == 1:
      value1 = pairValue(player1[n])
      value2 = pairValue(player2[n])
      if value1 > value2:
        win1 += 1
    else:
      ties += 1
      print "Players Tie"
      print value1, " ", player1[n], " ", value2, " ", player2[n]


print "GG"
print "Player 1 ins ", win1, " times"
print "There are ", str(ties), " games to resolve"
print str(len(player1)), " games played"

