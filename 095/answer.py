
def properDivisors(a):
    rtn = []
    for b in range(1, (a/2)+1):
        if a % b == 0:
            rtn.append(b)
            rtn.append(a/b)
    if a in rtn:
        rtn.remove(a)
    return sorted(set(rtn))


def sumProperDivisors(a):
    return sum(properDivisors(a))


def isPerfectNumber(a):
    return a == sumProperDivisors(a)


def genChain(a, LIMIT):
    chain = []
    _a = sumProperDivisors(a)
    while not _a in chain:

        # Limit set by question
        if _a > LIMIT:
            return chain

        chain.append(_a)
        _a = sumProperDivisors(_a)
    chain.append(_a)
    return chain


# Init values
max_length = 0
min_member = 0
LIMIT = 1000000
ignore = [1] * LIMIT

for a in range(0, LIMIT):
    ignore[a] = 0

for a in range(1, LIMIT):
    if ignore[a] == 1:
        continue
    print "a: " + str(a),
    print " max_length: " + str(max_length),
    print " min_member: " + str(min_member)

    chain = genChain(a, LIMIT)
    length = len(chain)

    for elem in chain:
        ignore[elem] = 1

    if length > 0 and chain[0] == chain[length-1]:
        if max_length < length:
            max_length = length
            min_member = min(chain)

