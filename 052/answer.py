
def sameDigits(a, b):
    return sorted(str(a)) == sorted(str(b))


def sameDigits_group(nums):
    for pos in range(1, len(nums)):
        if not sameDigits(nums[0], nums[pos]):
            return False
    return True

a = 0
while True:
    a += 1
    if sameDigits_group([a*1, a*2, a*3, a*4, a*5]):
        print a
        break
