import sys, os
import itertools
from numpy import prod

print "Template script that will calculate primes up to a limit"

limit = 100000000
candidates = [1] * limit
primes = [] * limit

for i in range(2,limit):
	j = 2
	while j * i < limit:
		candidates[i*j] = 0
		j += 1

print "Printing primes"
count = 0
for i in candidates:
	if i == 1:
		primes.append(count)
	count += 1


def productOfList(n,l):
	rtn = 1
	for d in l:
		if d > 0: rtn = rtn * d
		if rtn > n: return 0
	return rtn


def findDivisors(n):
	divisors = []
	_primes = []

	# Isolate primes
	for p in primes:
		if p > n/2: break
		if p == 0: continue
		if n % p > 0: continue

		if p == 1:
			divisors.append(0)
			_primes.append((p,0))
			continue

		_p = p
		while _p < n and n % _p == 0:
			divisors.append(_p)
			_primes.append((_p,0))
			_p = _p * p

	# Itertool
	for l in itertools.product(*_primes):
		_div = productOfList(n,l)
		if _div == 0: continue
		if n % _div == 0:
			divisors.append(productOfList(n,l))

	return set(divisors)

g = lambda n,d: d+n/d


""" MAIN """

print "Computing solution"

for n in range(1,limit):
	divisors = findDivisors(n)
	allprime = True

	for d in divisors:
		if d == 0: continue
		p = g(n,d)
		if not p in primes: allprime = False

	if allprime:
		print "All Prime",
		print n


