import sys, os

limit = 100000000
candidates = [1] * limit

for i in range(2,limit):
	j = 2
	while j * i < limit:
		candidates[i*j] = 0
		j += 1

print "Printing primes"
count = 0
primes = []
for i in candidates:
	if i == 1:
		primes.append(count)
	count += 1
print "Primed!"


def findDivisors(number):
	divs = []
	for n in range(1,number/2):
		if number % n == 0:
			divs.append(n)
			divs.append(number/n)
	return sorted(set(divs))

g = lambda n: map(lambda d: (d+n/d) in primes, findDivisors(n))

sum = 0
for n in range(1,limit):
	if n % 100 == 0:
		print n

	if not False in g(n):
		print n,
		sum += n
		print str(sum)

		
