def IsPythagorean(a,b,c):
  return ( pow(a,2) + pow(b,2) ) == pow(c,2)

for a in range(1,1000):
  for b in range(1,1000-a):
    for c in range(1,1000-b):
      if IsPythagorean(a,b,c):
        if a+b+c == 1000:
          print a*b*c
          
