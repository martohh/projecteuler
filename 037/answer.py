import sys, os

print "Template script that will calculate primes up to a limit"

limit = 1000
candidates = [1] * limit

for i in range(2,limit):
	j = 2
	while j * i < limit:
		candidates[i*j] = 0
		j += 1

print "Printing primes"
count = 0
for i in candidates:
	if i == 1:
		print count
	count += 1

