import java.util.*;

class answer {

	private static int limit = 999999;
	private static BitSet primes = new BitSet(limit);

	/* Init primes bitset array */
	public static void calcPrimes () {

		/* Init primes array of boolean */
		for (int i=0; i<limit; i++)
			primes.set(i);

		/* Calculate Primes */
		for (int i=2; i<limit; i++)
			for (int j=2; j*i<limit; j+=1)
				primes.clear(i*j);

		// 1 is not considered a prime number
		primes.clear(1);
	}

	/*	Print Primes
		Start from 1 and continune until limit */
	public static void printPrimes () {
		for (int i=0;i<limit;i++)
			if (primes.get(i))
				System.out.println(i);
	}

	/*	Cascade truncation to single digit
		Recursive truncation */
	public static boolean cascadeTruncLeft (int number) {
		if (!primes.get(number))
			return false;
		else if (number < 10 && primes.get(number))
			return true;
		else
			return cascadeTruncLeft(truncateLeftDigit(number));
	}

	/*	Cascade truncation to single digit
		Recursive truncation */
	public static boolean cascadeTruncRight (int number) {
		if (!primes.get(number))
			return false;
		else if (number < 10 && primes.get(number))
			return true;
		else
			return cascadeTruncRight(truncateRightDigit(number));
	}

	/*	Remove the most significant (left) digit from the given number. 
		Digit is lost in the process */
	public static int truncateLeftDigit (int number) {
		int power = 0;

		while (Math.pow(10,power) < number)
			power += 1;

		return number % (int)(Math.pow(10,power-1));
	}

	/*	Remove the least significant (right) digit from the given number. 
		Digit is lost in process */
	public static int truncateRightDigit(int number) {
		return number / 10;
	}

	public static void main(String[] args) {

		int count = 0;
		int sum = 0;

		calcPrimes();

		for (int number=10; number<limit; number+=1) {

			if (cascadeTruncLeft(number) && cascadeTruncRight(number)) {
				
				count += 1;
				sum += number;

				System.out.print(number);
				System.out.println(" is a truncatable prime!");
			}
		}

		System.out.print("\nWe have calculated ");
		System.out.print(count);
		System.out.println(" primes.");

		System.out.print("The sum of these primes is ");
		System.out.println(sum);
	}
}
