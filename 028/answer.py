
size = 1001 
grid = []

def printGrid():
  for x in range(0,size):
    for y in range(0,size):
      print str(grid[x][y]).zfill(4),
    print

def initGrid():
  for x in range(0,size):
    row = []
    for y in range(0,size):
      row.append(0)
    grid.append(row)

def seedGrid():
  cur_value = pow(size,2)
  direction = 0
  step = [[0,-1],[1,0],[0,1],[-1,0]]
  cur_x = size-1
  cur_y = 0

  grid[cur_y][cur_x] = cur_value
  cur_value -= 1

  while cur_value > 0:
    try:
      next_step = step[direction]
      next_x = cur_x + next_step[1]
      next_y = cur_y + next_step[0]

      if grid[next_y][next_x] > 0:
        direction = (direction+1) % 4
        continue

      grid[next_y][next_x] = cur_value
      cur_value -= 1
      cur_x = next_x
      cur_y = next_y
    except:
      direction = (direction + 1) % 4

def sumDiagonals():
  sum = -1 
  for x in range(0,size):
    sum += grid[x][x]
    sum += grid[x][size-1-x]
  return sum

""" 
  Start of main
"""
initGrid()
seedGrid()
printGrid()
print sumDiagonals()
