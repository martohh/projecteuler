class solution {

	public static Boolean isPrime(int n) {
		if (n == 2)
			return true;

		for (int x=2; x<n-1; x++)
			if (n % x == 0)
				return false;
		return true;
	}

	public static void main(String[] args) {
		long sum = 0;
		for (int x=2; x<2000000; x++) {
			if (isPrime(x))
				sum += x;
			if (x % 100000 == 0)
				System.out.printf("Past %d\tSum: %d\n",x, sum);

		}
		System.out.printf("Sum: %d\n",sum);
	}
}