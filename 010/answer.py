import sys, os
import thread
import time

def isPrime(x):
  y=2
  while y < x-1:
    if x % y == 0:
      return False
    y += 1
  return True

# Define a function for the thread
def sum_primes( threadName, threadNumber, _min, _max ):
  global working
  n = _min

  while n <= _max:
    if n % 10000 == 0:
      print threadName + ": " + str(n) + " Sum: " + str(sum[threadNumber])

    if isPrime(n):
      sum[threadNumber] += n
    n += 1
  
  print sum[threadNumber]
  print

  working -= 1
  
working=0
sum=[0,0,0,0]

# Create two threads as follows
"""
          0
    700,000
  1,600,000
  1,750,000
  2,000,000
"""
try:
  thread.start_new_thread( sum_primes, ("Thread-0", 0, 1, 700000) )
  working += 1
  thread.start_new_thread( sum_primes, ("Thread-1", 1, 700000, 1600000) )
  working += 1
  thread.start_new_thread( sum_primes, ("Thread-2", 2, 1600000, 1750000) )
  working += 1
  thread.start_new_thread( sum_primes, ("Thread-1", 1, 1750000, 2000000) )
  working += 1

except:
   print "Error: unable to start thread"

while working > 0:
   pass

print sum[0] + sum[1] + sum[2] + sum[3]

sys.exit()


sum = 0
n = 2
while n < 2000000:
  if isPrime(n):
    print "found: " + str(n)
    sum += n
  n += 1

print "Sum: " + str(sum)
