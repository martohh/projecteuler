bitmap = 0

def SetBit(num,bitmap):
  mask = pow(2,num-1)
  return bitmap | mask

def ReadBit(num,bitmap):
  return (bitmap >> num-1) == 1

bitmap = SetBit(1000,bitmap)
for n in range(990,1001):
  print ReadBit(n,bitmap)

print bitmap
