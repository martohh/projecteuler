import sys, os

# Proper Divisor: a set of all divisors of a number excluding the number itself
# Abundant Number: the sum of a number's proper divisors is greater than the number itself

def findDivisors(number):
	divs = []
	x = 1
	while x < number-1:
		if number % x == 0:
			divs.append(x)
		x += 1
	return divs

def sumList(list):
	sum = 0
	for i in list:
		sum += i
	return sum

def printList(list):
	for i in list:
		print i,
	print

def isAbundantNumber(number):
	sum = sumList(findDivisors(number))
	if number < sum:
		return True
	return False

def isPerfectNumber(number):
	sum = sumList(findDivisors(number))
	if number == sum:
		return True
	return False


""" MAIN """

print "Prepare numbers"
n=[]
for x in range(0,28123):
	n.append(x)

print "Searching for Abundant Numbers"
abundant = []
for x in range(0, 28123):
	if x % 10000 == 0:
		print x
	if isAbundantNumber(x):
		abundant.append(x)

print "There are " + str(len(abundant)) + " numbers found."

print "Removing numbers that are sum of two abundant numbers"
for a in range(0,len(abundant)):

	if a % 10000 == 0:
		print a

	for b in range(0,len(abundant)):
		if (abundant[a]+abundant[b]) < 28123:
			n[abundant[a]+abundant[b]] = 0

print "Printing solutions"
sum = 0
for x in range(0,28123):
	if n[x] < 28123:
		print n[x]
		sum += n[x]

print sum


