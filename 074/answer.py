import sys, os, re, math

def sumFactorial(num):
	_sum = 0
	num_parts = str(num)

	for num_part in num_parts:
		math.factorial(int(num_part))
		_sum += math.factorial(int(num_part))

	return _sum

def chainFactorial(num):

	_chain = []
	_chain.append(num)
	_start = num

	while True:
		_num = sumFactorial(num)

		if _num == _start:
			_chain.append(_num)
			return {'chain': _chain, 'msg': 'complete non-repeating chain found', 'count': len(set(_chain))}

		if _num in _chain:
			_chain.append(_num)
			return {'chain': _chain, 'msg': 'detected loop', 'count': len(set(_chain))}

		_chain.append(_num)
		num = _num

		if len(set(_chain)) > 7:
			return {'chain': _chain, 'msg': 'more than six', 'count': len(set(_chain))}

num = 1
count = 0
limit = 1000000

while True:

	result = chainFactorial(num)

	if result['count'] == 6:
		count += 1
		print result['chain']

	if num > limit:
		break

	if num % 10000 == 0:
		print 'attempt', num
	num += 1

print count 
