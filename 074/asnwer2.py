import math

COUNT = 0
LIMIT = 1000000
CHAIN_LENGTH = 60


def sumDigitFact(a):
    sum = 0
    for d in sorted(str(a)):
        sum += math.factorial(int(d))
    return sum


def getChain(a):
    chain = []
    chain.append(a)
    _a = sumDigitFact(a)
    while not _a in chain:

        # Limit set in question
        if len(chain) > CHAIN_LENGTH + 1:
            return []

        chain.append(_a)
        _a = sumDigitFact(_a)
    return chain


for a in range(1, LIMIT):
    _a = getChain(a)
    length = len(_a)

    if length == 60:
        COUNT += 1


print "Answer to 074"
print COUNT
