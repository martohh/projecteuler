
def SumOfSquares(a,b):
  s = 0
  for i in range(a,b+1):
    s += i*i
  return s

def SquareOfSums(a,b):
  s = 0
  for i in range(a,b+1):
    s += i
  return s*s

print SquareOfSums(1,100) - SumOfSquares(1,100)

