import sys
sys.path.append('/home/marty/scripts/projecteuler/lib/')
import common


MaxRange = 10001
print "Loading Primes... ",
primes = common.LoadPrimes(0)
print "Primed!"
result = []

for a in range(1,MaxRange):
  divisors_a = common.FastFindDivisors(a,primes)
  b = sum(divisors_a)
  divisors_b = common.FastFindDivisors(b,primes)

  if sum(divisors_a) == b and sum(divisors_b) == a and a > 1 and b > 1 and not a == b:
    print a, ": ",divisors_a, " ", sum(divisors_a)
    print b, ": ",divisors_b, " ", sum(divisors_b)
    print 
    if a < MaxRange:
      result.append(a)
    if b < MaxRange:
      result.append(b)


print set(result)
print "Sum: ", sum(set(result))

