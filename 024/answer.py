import itertools, sys, os

list = itertools.permutations('0123456789', 10)
numbers = []

count = 0
for l in list:
	n = 0
	pow = 1000000000

	for d in l:
		n += int(d) * pow
		pow /= 10

	numbers.append(n)

numbers = sorted(numbers)

count = 1
for number in numbers:
	if count % 10000 == 0:
		print str(count) + " " + str(number)
	
	if count % 1000000 == 0:
		print str(count) + " " + str(number)
		sys.exit()

	count += 1
