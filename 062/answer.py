import sys, os, math, itertools

cubic = lambda n: n == round(n**(1/3.0))**3

n = 0
#       1891000000
limit = 5000000000

while n < limit:

	if n % 1000000 == 0:
		print n

	solution = []

	if cubic(n):

		numChar = len(str(n))
		for i in itertools.permutations(list(str(n))):
			a = int("".join(i))

			if not len(str(a)) == numChar:
				continue

			if cubic(a):
				solution.append(a)
		solution = set(solution)

		if len(solution) == 5:
			print solution
			sys.exit()

	n += 1

print "done"
