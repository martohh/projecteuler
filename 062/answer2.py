import os, sys, itertools

cube = lambda n: n**3
isCube = lambda n: n == round(n**(1/3.0))**3

add = lambda x, y: int(x) + int(y)
sumDigits = lambda n: reduce(add,list(str(n)))
sortDigits = lambda n: "".join(sorted(list(str(n))))

numbers = {}
n = 2150

for n in range(2,99999):

	a = cube(n)
	hash = sortDigits(a)
	if hash in numbers:
		values = numbers[hash]
		values.append(a)
		if len(values) == 5:
			print n
			print values
			sys.exit()
		numbers[hash] = values
	else:
		numbers[hash] = [a]


