from collections import Counter

LIMIT = 1000


def primesWithRepeatingDigits(d):
    CANDIDATES = [1] * LIMIT

    for i in range(2, LIMIT):
        j = 2
        while j * i < LIMIT:
            CANDIDATES[i*j] = 0
            j += 1

    count = 0
    for i in CANDIDATES:
        if i == 1:
            print count
        count += 1


def repeatingDigits(a):
    return Counter(sorted(str(a)))


print repeatingDigits(10)
