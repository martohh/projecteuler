import sys, os

limit = 99999
candidates = [1] * limit

""" Generate Primes """
for i in range(2,limit):
	j = 2
	while j * i < limit:
		candidates[i*j] = 0
		j += 1

""" Quick Prime Check """
def isPrime (number):
	if candidates[number] == 1:
		return True
	else:
		return False

seed = 123
length = len(str(seed)) + 2

""" Generate positions """
for (x,y) in [(x,y) for x in range (0,length) for y in range(x+1,length)]:

	""" Number to replace with """ 
	for n in range(0,9):
	

