import sys, os, re

def isPalindromic (n):
	l = len(str(n))-1
	for d in range(0,l/2+1):
		if not str(n)[d] == str(n)[l-d]:
			return False
	return True

def reverse (n):
	return int(str(n)[::-1])

def test(n):
	for count in range(1,50):
		r = reverse(n)
		if isPalindromic(int(n)+int(r)):
			return count
		else:
			n += r	
	return -1
	
Lychrel = 0
for n in range(10,10001):
	r = test(n)
	print n,
	if r > 0:
		print "took %s iterations to find Palindromic sum"%r
	else:
		print "cannot be mapped to a Palindromic number"
		Lychrel += 1

print "Lychrel (answer): " + str(Lychrel)

