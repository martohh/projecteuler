import sys
import re
import os

### Dynamic programming
# two arrays. Distance (cost) and number of solutions (paths) to a gien point
# spider can only walk on walls, therefore space in the middle of the room is irrelavant
# 


# Declare objects and set variables
M=10
xM=M
yM=M
zM=M

cost = [[]]
paths = [[]]


# Initialize things
cost = [[[0 for x in range(xM)] for y in range(yM)] for z in range(zM)]
paths = [[[0 for x in range(xM)] for y in range(yM)] for z in range(zM)]


def findCheapestNode (x, y, z):

	delta_cost = []
	for delta_x in range(-1, 1):
		if x + delta_x < 0 or x + delta_x > xM-1:
			continue
		for delta_y in range(-1, 1):
			if y + delta_y < 0 or y + delta_y > yM-1:
				continue 
			for delta_z in range(-1, 1):
				if z + delta_z < 0 or z + delta_z > zM-1:
					continue 

				# Remove origin
				if delta_x==0 and delta_y==0 and delta_z==0:
					continue

				# Remove if point is in air
				if x in [0, xM-1] or y in [0, yM-1] or z in [0, zM-1] or cost[x + delta_x][y + delta_y][z + delta_z] == 0:
					delta_cost.append(cost[x + delta_x][y + delta_y][z + delta_z])

	if len(delta_cost) > 0:
		min_cost = min(delta_cost)
		return [min(delta_cost) + 1, delta_cost.count(min_cost)]

	return [-10, -10]


cost[0][0][0] = 0

for x in range(0, xM):
	for y in range(0, yM):
		for z in range(0, zM):
			if x==0 or y==0 or z==0 or x==xM-1 or y==yM-1 or z==zM-1:
				c = findCheapestNode(x,y,z)
				print "node: (", x, y, z, ") cost: (", c[0], ") paths: (", c[1], ")"
				cost[x][y][z] = findCheapestNode(x,y,z)[0]
				paths[x][y][z] = findCheapestNode(x,y,z)[1]

