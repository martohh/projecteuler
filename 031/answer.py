import sys, re, os

denomination = [1,2,5,10,20,50,100,200]
coin_bag = [0,0,0,0,0,0,0,0]
most_coins = [200,100,40,20,10,5,2,1]

def count_coins():
	rtn = 0
	for key, value in enumerate(denomination):
		rtn += coin_bag[key] * denomination[key]
	return rtn

def add_pent():
	pos = 0
	while True:
		if coin_bag[pos] < most_coins[pos]:
			coin_bag[pos] += 1
			return
		coin_bag[pos] = 0
		pos += 1

answer = 0

#for x in range(0,pow(10,10)):
while coin_bag[7] < 1:
	add_pent()
	if count_coins() == 200:
		print coin_bag 
		answer += 1

print answer


