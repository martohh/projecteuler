
def divisible(number,ceiling):
  for a in range(1,ceiling):
    if (number % a) > 0:
      return False
  return True


for b in range(1,10000000):
  for n in range((b-1)*10000,b*10000):
    if divisible(n,20):
      print n
      break
