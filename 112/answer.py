def IsIncreasing(number):
  prev = 10
  while number > 0: 
    if prev < number % 10:
      return False
    prev = number % 10
    number /= 10
  return True

def IsDecreasing(number):
  prev = 0
  while number > 0:
    if prev > number % 10:
      return False
    prev = number % 10
    number /= 10
  return True

def IsBouncy(number):
  if IsIncreasing(number):
    return False
  if IsDecreasing(number):
    return False
  return True

n = 0
bouncy = []
while True:
  if IsBouncy(n):
    bouncy.append(n)
  n += 1

  print n, " ", len(bouncy), " ", 100.0 * len(bouncy) / n, "%"
  if 100.0 * (float(len(bouncy)) / float(n)) > 99.0:
    break

  if n == 21780:
    print n, " ", len(bouncy), " ", 100.0 * len(bouncy) / n, "%"
    break
   

print "Answer: ", n
