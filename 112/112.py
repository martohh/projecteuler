import sys, os, re

isBouncy = []
count = 0
limit = 99

def isAscending(number):

	_n = 0
	for n in str(number):
		if n < _n:
			return False
		_n = n

	return True
	
def isBouncy(number):
	
	# Check normal
	if isAscending(number):
		return False
	# Check reverse
	if isAscending(str(number)[::-1]):
		return False
	return True

n = 0
percent = 0.0
while percent < limit:
	if isBouncy(n):
		count += 1
		percent = float(count) / float(n) * 100
		print n, count, percent
	n += 1

