import java.util.*;

class answer {

	private static int limit = 999999;
	private static BitSet primes = new BitSet(limit);

	/* Init primes bitset array */
	public static void calcPrimes () {

		/* Init primes array of boolean */
		for (int i=0; i<limit; i++)
			primes.set(i);

		/* Calculate Primes */
		for (int i=2; i<limit; i++)
			for (int j=2; j*i<limit; j+=1)
				primes.clear(i*j);

		// 1 is not considered a prime number
		primes.clear(1);
	}

	/*	Print Primes
		Start from 1 and continune until limit */
	public static void printPrimes () {
		for (int i=0;i<limit;i++)
			if (primes.get(i))
				System.out.println(i);
	}

	/* Check next two in series */
	public static boolean checkSeries(int number, int increment) {
		if (number + 2 * increment > limit)
			return false;

		if (!(primes.get(number) && primes.get(number + increment) && primes.get(number + 2 * increment)))
			return false;

		if (!(isPermutation(number, number + increment, number + 2 * increment)))
			return false;

		return true;
	}

	/* Find next prime */
	public static int findNextPrime(int start) {
		int cur = 0;
		for (cur = start + 1; cur < limit; cur += 1)
			if (primes.get(cur))
				return cur;
		return -1;
	}

	public static List<Integer> decompileNumber(int number) {
		List<Integer> rtn = new ArrayList<Integer>();

		for (int n=number; n>0; n/=10)
			rtn.add(n % 10);
		
		Collections.sort(rtn);
		return rtn;
	}

	/* Determine if the three provided numbers are permutations of one and the other */
	public static boolean isPermutation(int a, int b, int c) {

		// Cannot be repetitions in numbers
		if (a == b || a == c || b == c)
			return false;

		List<Integer> a_array = new ArrayList<Integer>();
		a_array = decompileNumber(a);

		List<Integer> b_array = new ArrayList<Integer>();
		b_array = decompileNumber(b);

		List<Integer> c_array = new ArrayList<Integer>();
		c_array = decompileNumber(c);

		// Ensure that the numbers are the same length
		if (a_array.size() != b_array.size())
			return false;
		if (a_array.size() != c_array.size())
			return false;

		// Compare Arrays, if original numbers are permutations, then each element should match
		for (int i=0; i<a_array.size(); i++) {
			if (a_array.get(i) != b_array.get(i))
				return false;
			if (a_array.get(i) != c_array.get(i))
				return false;
		}

		return true;
	}

	public static void main (String[] args) {

		calcPrimes();

		for (int base = 1487; base < limit; base += 1) {
			for (int inc=1; base + 2*inc < limit; inc = findNextPrime(base + inc) - base)
				if (checkSeries(base, inc)) {

					System.out.print(base);
					if (primes.get(base))
						System.out.print("*\n");
					System.out.print(base + inc);
					if (primes.get(base + inc))
						System.out.print("*\n");
					System.out.print(base + 2 * inc);
					if (primes.get(base + 2 * inc))
						System.out.print("*\n");

					System.out.println("inc: " + inc);
					System.out.println("\n\n");

				}
		}

	}
}