# Problem 49

import sys, os, itertools

primes = [1]*pow(10,3)

for a in range(2, len(primes)):
	b = a
	while b+a < len(primes):
		b += a
		primes[b] = 0


permPrimes = []
n = 0
while n < len(primes):
	if primes[n] == 1:
		row = []
		for b in ["".join(a) for a in list(itertools.permutations(list(str(n))))]:
			row.append(int(b))
		if not row in permPrimes:
			row = set(row)
			permPrimes.append(sorted(row))
	n += 1

permPrimes = sorted(permPrimes)

for p in permPrimes:
	print p
