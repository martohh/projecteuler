from collections import Counter

LIMIT = 10000
THRESHOLD = 999

triangle = [1] * LIMIT
square = [1] * LIMIT
pentagonal = [1] * LIMIT
hexigonal = [1] * LIMIT
heptigonal = [1] * LIMIT
octagonal = [1] * LIMIT

fn_triangle = lambda n: n*(n+1)/2
fn_square = lambda n: n*n
fn_pentagonal = lambda n: n*(3*n-1)/2
fn_hexigonal = lambda n: n*(2*n-1)
fn_heptigonal = lambda n: n*(5*n-3)/2
fn_octagonal = lambda n: n*(3*n-2)

# Init bit vectors
for a in range (0, LIMIT):
    triangle[a] = 0
    square[a] = 0
    pentagonal[a] = 0
    hexigonal[a] = 0
    heptigonal[a] = 0
    octagonal[a] = 0

# Calculate bit vectors
for a in range(0, LIMIT):
    _triangle = fn_triangle(a)
    if _triangle < LIMIT and _triangle > THRESHOLD:
        triangle[_triangle] = 1
    _square = fn_square(a)
    if _square < LIMIT and _square > THRESHOLD:
        square[_square] = 1
    _pentagonal = fn_pentagonal(a)
    if _pentagonal < LIMIT and _pentagonal > THRESHOLD:
        pentagonal[_pentagonal] = 1
    _hexigonal = fn_hexigonal(a)
    if _hexigonal < LIMIT and _hexigonal > THRESHOLD:
        hexigonal[_hexigonal] = 1
    _heptigonal = fn_heptigonal(a)
    if _heptigonal < LIMIT and _heptigonal > THRESHOLD:
        heptigonal[_heptigonal] = 1
    _octagonal = fn_octagonal(a)
    if _octagonal < LIMIT and _octagonal > THRESHOLD:
        octagonal[_octagonal] = 1


def isOnePolygonalType(a):
    return 1 == (triangle[a] + square[a] + pentagonal[a] + hexigonal[a] + heptigonal[a] + octagonal[a])


def polygonalTypes(a):
    types = []
    if triangle[a] == 1:
        types.append("triangle")
    if square[a] == 1:
        types.append("square")
    if pentagonal[a] == 1:
        types.append("pentagonal")
    if hexigonal[a] == 1:
        types.append("hexigonal")
    if heptigonal[a] == 1:
        types.append("heptigonal")
    if octagonal[a] == 1:
        types.append("octagonal")
    return types


def noPolygonalTypeOverlap(a, b, c=None, d=None, e=None, f=None):
    types = []
    types = polygonalTypes(a)
    types += polygonalTypes(b)

    if not c == None:
        types += polygonalTypes(c)
    if not d == None:
        types += polygonalTypes(d)
    if not e == None:
        types += polygonalTypes(e)
    if not f == None:
        types += polygonalTypes(f)

    for elem in Counter(types):
        if Counter(types)[elem] > 1:
            return False

    return True


def isCyclic(a, b, c=None, d=None, e=None, f=None):
    if not str(a)[2:] == str(b)[0:2]:
        return False


    if c is None:
        # compare b to a

        return True

    else:
        # compare b to c


    if d is None:
        # Compare c to a

        return True

    else:
        # compare c to d

    if e is None:
        # compare d to a

        return True

    else:
        # compare d to e

    if f is None:
        # Compare e to a

        return True

    # compare f to a



    if c is not None and not str(b)[2:] == str(c)[0:2]:
        return False
    elif c is None and not str(b)[2:] == str(a)[0:2]:


    if d is not None and not str(c)[2:] == str(d)[0:2]:
        return False
    elif c is None and not str(c)[2:] == str(a)[0:2]:


    if e is not None and not str(d)[2:] == str(e)[0:2]:
        return False
    elif c is None and not str(d)[2:] == str(a)[0:2]:


    if f is not None and not str(e)[2:] == str(f)[0:2]:
        return False
    elif c is None and not str(e)[2:] == str(a)[0:2]:


    if a is not None and not str(f)[2:] == str(a)[0:2]:
        return False
    elif c is None and not str(f)[2:] == str(a)[0:2]:


    return True


def isCyclic(a, b, c):
    if not str(a)[2:] == str(b)[0:2]:
        return False
    if not str(b)[2:] == str(c)[0:2]:
        return False
    if not str(c)[2:] == str(a)[0:2]:
        return False
    return True


print noPolygonalTypeOverlap(8128, 8128, 123)
print noPolygonalTypeOverlap(8128, 2882, 8281)


"""
for a in range(THRESHOLD, LIMIT):
    for b in range(THRESHOLD, LIMIT):
        if b in [a]:
            continue
        for c in range(THRESHOLD, LIMIT):
            if c in [a,b]:
                continue
            for d in range(THRESHOLD, LIMIT):
                if d in [a,b,c]:
                    continue
                for e in range(THRESHOLD, LIMIT):
                    if e in [a,b,c,d]:
                        continue
                    for f in range(THRESHOLD, LIMIT):
                        if f in [a,b,c,d,e]:
                            continue

                        print a, b, c, d, e, f
"""
