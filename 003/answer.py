
def isPrime(x):
	y = 2
	while y < x-1:
		if x % y == 0:
			return False
		y += 1
	return True

number = 600851475143 

n = 1
while n < 600851475143:
	if 600851475143 % n == 0:
		if isPrime(n):
			print n
	n += 1

# print isPrime(600851475143)
