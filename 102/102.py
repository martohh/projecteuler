import sys
import re
import os

def findSlope(x1, y1, x2, y2):
	try:
		return 1.0 * (float(y2) - float(y1)) / (float(x2) - float(x1))
	except:
		print "could not calculate slope"
		return None

def b_intercept(x1, y1, x2, y2):
	m = findSlope(x1, y1, x2, y2)
	try:
		return float(y1) - (float(m) * float(x1))
	except:
		print "could not calculate b"
		return None

def x_intercept(x1, y1, x2, y2):
	try:
		m = findSlope(x1, y1, x2, y2)
		b = b_intercept(x1, y1, x2, y2)
		return float(-b) / float(m)
	except:
		print "could not calculate x intercept"
		return None

def y_intercept(x1, y1, x2, y2):
		return b_intercept(x1, y1, x2, y2)

def verify_intercept(intercept, p1, p2, p3):
	if min(float(p1), float(p2), float(p3)) < float(intercept) and float(intercept) < max(float(p1), float(p2), float(p3)):
		return intercept
	print "could not validate intercept, intercept outside bounds"
	return None

def check_origin(points):

	x_intercepts, y_intercepts = [], []
	
	intercept = x_intercept(points[0], points[1], points[4], points[5])
	if intercept is not None and verify_intercept(intercept, points[0], points[2], points[4]):
		x_intercepts.append(intercept)
	intercept = x_intercept(points[0], points[1], points[2], points[3])
	if intercept is not None and verify_intercept(intercept, points[0], points[2], points[4]):
		x_intercepts.append(intercept)
	intercept = x_intercept(points[2], points[3], points[4], points[5])
	if intercept is not None and verify_intercept(intercept, points[0], points[2], points[4]):
		x_intercepts.append(intercept)

	intercept = y_intercept(points[0], points[1], points[2], points[3])
	if intercept is not None and verify_intercept(intercept, points[1], points[3], points[5]):
		y_intercepts.append(intercept)
	intercept = y_intercept(points[0], points[1], points[4], points[5])
	if intercept is not None and verify_intercept(intercept, points[1], points[3], points[5]):
		y_intercepts.append(intercept)
	intercept = y_intercept(points[2], points[3], points[4], points[5])
	if intercept is not None and verify_intercept(intercept, points[1], points[3], points[5]):
		y_intercepts.append(intercept)	

	print "x_intercepts: ", x_intercepts
	print "y_intercepts: ", y_intercepts

	if len(x_intercepts) == 0 or len(y_intercepts) == 0:
		if len(x_intercepts) == 0:
			print "no x intercepts"

		if len(y_intercepts) == 0:
			print "no y intercepts"
		return None

	return min(x_intercepts) <= 0.0 and max(x_intercepts) >= 0.0 and min(y_intercepts) <= 0.0 and max(y_intercepts) >= 0.0

#line = "-175,41,-421,-714,574,-645"
#line = "-340,495,-153,-910,835,-947"
count = 0
number = 0

with open("C:\Users\Mike\Desktop\\triangles.txt") as f:
	for line in f:
		number += 1 
		points = line.replace("\n","").split(",")
		contains_origin = check_origin(points)
		if contains_origin == True:
			count += 1

			print points, contains_origin
			print "(", count, " / ", number, ")"
			print points[0], "\t", points[1]
			print points[2], "\t", points[3]
			print points[4], "\t", points[5]
		print 

print count, " / ", number

