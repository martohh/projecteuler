import sys
import re
import os

def area(Ax, Ay, Bx, By, Cx, Cy):
	return abs(((Ax * (By - Cy)) + (Bx * (Cy - Ay)) + (Cx * (Ay - By)) ) * 0.5)

def validate(points):
	[Ax, Ay, Bx, By, Cx, Cy] = points 
	originTriangleArea =  area(float(Ax), float(Ay), float(Bx), float(By), 0.0, 0.0) 
	originTriangleArea += area(float(Ax), float(Ay), float(Cx), float(Cy), 0.0, 0.0) 
	originTriangleArea += area(float(Bx), float(By), float(Cx), float(Cy), 0.0, 0.0)
	triangleArea = area(float(Ax), float(Ay), float(Bx), float(By), float(Cx), float(Cy))	
	return originTriangleArea == triangleArea

count = 0
number = 0

with open("C:\Users\Mike\Desktop\\triangles.txt") as f:
	for line in f:
		number += 1 

		points = line.split(",")
		contains_origin = validate(points)

		if contains_origin == True:
			count += 1

print count, " / ", number
