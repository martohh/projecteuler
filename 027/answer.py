import sys, os


def equation(n, a=0, b=0):
  a = abs(a)
  b = abs(b)
  return pow(n,2) + (a*n) + b
  #return pow(n,2) + n + 41   # Testing equation


def isPrime_calc(n):
  a = 2
  while a < n:
    if n % a == 0:
      return False
    a += 1
  return True


def isPrime(n):
  global primes

  if n > len(primes):
    print "not enough primes calcualted"
    print "Requesting to check %s"%(str(n))
    print "Calculated up to %s"%(str(len(primes)))
    print
    print

    sys.exit()

  if primes[n] == 1:
    return True
  return False


def sieveOfEratosthenes(limit=1000):
  global primes
  primes = []
  for n in range(0,limit):
    primes.append(1)

  for n in (2,3,5,7,13):
    k = 0
    while k < limit:
      primes[k] = 0
      k += n

  n = 0
  while n < limit:
    if primes[n] > 0:
      if not isPrime_calc(n):
        primes[n] = 0
    n += 1


def printPrimes():
  global primes
  n=0
  c=1
  while n < len(primes):
    if primes[n] == 1:
      print str(n) + ' ',
      if c % 25 == 0:
        print
      c += 1
    n += 1
  print 
  print


primes = []
sieveOfEratosthenes(999999)
#printPrimes()
print "primed!"

max_run=0
max_a=0
max_b=0
max_n=0
n=0
run=0


for a in range(-1000, 1000):
  for b in range(-1000, 1000):
    run=0
    for n in range(0, 50):
      value = equation(n, a, b)
      print str(a) + ":" + str(b) + ":" + str(n) + ":" + str(value)
      if isPrime(value):
        run += 1
      else:
        if max_run < run:
          max_run = run
          max_a = a
          max_b = b
        run = 0

    print str(max_a) + " " + str(max_b) + " " + str(max_run)

