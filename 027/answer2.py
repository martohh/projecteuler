import sys, os

print "Template script that will calculate primes up to a limit"

limit = 10000
candidates = [1] * limit
e = lambda n: pow(n,2) + a * n + b

for i in range(2,limit):
	j = 2
	while j * i < limit:
		candidates[i*j] = 0
		j += 1

print "Printing primes"
count = 0
primes = []
for i in candidates:
	if i == 1 and count > 1:
		primes.append(count)
	count += 1
print "Primed!"

max_count = 0
for (a,b) in [(a, b) for a in range(-1000,1000) for b in range(-1000,1000)]:
	count = 0
	n = 0
	while n < 100:
		value = e(n)
		n += 1
		if value in primes:
			count += 1
		else:
			if count > max_count:
				max_count = count
				print "count: ",
				print count,
				print "a: ",
				print a,
				print "b: ",
				print b
			count = 0
			n = 999

