def Mask(number,digits):
  if len(str(number)) < digits:
    return number
  return int(str(number)[-digits:])

n = 0
result = 1
while n < 7830457:
  result *= 2
  result = Mask(result, 10)
  if n % 10000 == 0:
    print result
  n += 1

result = 28433*result + 1
print Mask(result,10)
